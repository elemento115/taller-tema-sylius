var gulp = require('gulp');
var changed = require('gulp-changed');
var concat = require('gulp-concat');
var imagemin = require('gulp-imagemin');
var jshint = require('gulp-jshint');
var minifycss = require('gulp-minify-css');
var notify = require('gulp-notify');
var rename = require('gulp-rename');
var sass = require('gulp-sass');
var uglify = require('gulp-uglify');

var destRelativePath = '../AppBundle/public/';
var destRelativeWebPath = '../../../../web/bundles/_themes/e115/e115-theme/app/';

gulp.task('images', function() {
    return gulp.src(['img/**'])
        .pipe(gulp.dest(destRelativePath+'img/'))
        .pipe(gulp.dest(destRelativeWebPath+'img/'))
        .pipe(notify({message: 'IMAGES OK'}));
});

gulp.task('imagesmin', function() {
    return gulp.src(['img/**'])
        .pipe(changed(destRelativePath+'img/'))
        .pipe(imagemin({optimizationLevel: 7}))
        .pipe(gulp.dest(destRelativePath+'img/'))
        .pipe(gulp.dest(destRelativeWebPath+'img/'))
        .pipe(notify({message: 'IMAGES MIN OK'}));
});

gulp.task('styles', function() {
    return gulp.src('scss/*.scss')
        .pipe(sass({includePaths: ['scss/modules']}))
        .pipe(gulp.dest('css'))
        .pipe(minifycss())
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest(destRelativePath+'css'))
        .pipe(gulp.dest(destRelativeWebPath+'css'))
        .pipe(notify({message: 'CSS OK'}));
});

gulp.task('hint', function() {
	return gulp.src('js/*.js')
		.pipe(jshint())
		.pipe(jshint.reporter('default'))
		.pipe(notify({message: 'HINT OK'}));
});

gulp.task('scripts', function() {
	return gulp.src('js/*.js')
		.pipe(rename({suffix: '.min'}))
		.pipe(uglify())
		.pipe(gulp.dest(destRelativePath+'js'))
		.pipe(gulp.dest(destRelativeWebPath+'js'))
		.pipe(notify({message: 'JS OK'}));
});

gulp.task('watch', function() {
	gulp.watch('img/**', ['images']);
	gulp.watch('scss/**/*.scss', ['styles']);
	gulp.watch('js/*.js', ['hint', 'scripts']);
});

gulp.task('default', ['watch']);

gulp.task('resources-install', ['images','imagesmin','styles','hint','scripts']);