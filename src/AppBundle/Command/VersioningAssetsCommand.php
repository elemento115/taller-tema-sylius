<?php

namespace AppBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Yaml\Yaml;

/**
 * Class CreateUserCommand
 * @package AppBundle\Command
 */
class VersioningAssetsCommand extends Command
{
    protected function configure()
    {
        $this
            ->setName('app:assets-change-version')
            ->setDescription('Chage version of assets in the parameter.yml')
            ->setHelp('Use it to change de version in the deployment')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $filename = 'app/config/parameters.yml';
        if (file_exists($filename)) {
            $value = Yaml::parse(file_get_contents($filename));
            if (isset($value['parameters']['assets_version'])) {
                $value['parameters']['assets_version'] = md5(uniqid(rand(), true));
                file_put_contents($filename, Yaml::dump($value));
            }
        }
    }
}